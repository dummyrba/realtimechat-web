<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=0.1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Chat App</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">           </head>
    </head>
    <body>
        <div class="wrapper">
            <section class="form signup">
                <header>Chat App</header>
                <form action="#">
                    <div class="error-txt">This an error message</div>
                    <div class="name-details">
                        <div class="field input">
                            <label>First Name</label>
                            <input type="text" placeholder="First Name">
                        </div>
                        <div class="field input">
                            <label>Last Name</label>
                            <input type="text" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="field input">
                        <label>Email Address</label>
                        <input type="text" placeholder="Enter your Email">
                    </div>
                    <div class="field input">
                        <label>Password</label>
                        <input type="password" placeholder="Enter new Password">
                        <i class="fa fa-eye"></i>
                    </div>
                    <div class="field image">
                        <label>Select Image</label>
                        <input type="file">
                    </div>
                    <div class="field button">
                        <input type="submit" value="Continue to chat">
                    </div>
                </form>
                <div class="link">already singed Up? <a href="#">Login Now</a></div>
            </section>
        </div>
        <script src="javascript/pass-show-hide.js"></script>
    </body>
</html>