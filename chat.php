<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=0.1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Chat App</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
    </head>
    <body>
        <div class="wrapper">
            <section class="chat-area">
                <header>
                    <a href="" class="back-icon"><i class="fa fa-arrow-left"></i></a>
                    <img src="photo.jpg" alt="">
                    <div class="details">
                        <span>Wakatobie</span>
                        <p>Active Now</p>
                    </div>
                </header>
                <div class="chat-box">
                    <div class="chat outgoing">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat incoming">
                        <img src="photo.jpg" alt="">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat outgoing">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat incoming">
                        <img src="photo.jpg" alt="">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat outgoing">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat incoming">
                        <img src="photo.jpg" alt="">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat outgoing">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="chat incoming">
                        <img src="photo.jpg" alt="">
                        <div class="details">
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                </div>
                <form action="#" class="typing-area">
                    <input type="text" placeholder="type a message here....">
                    <button><i class="fa fa-paper-plane"></i></button>
                </form>
            </section>
        </div>
    </body>
</html>