<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=0.1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Chat App</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">           </head>
    </head>
    <body>
        <div class="wrapper">
            <section class="users">
                <header>
                    <div class="content">
                        <img src="photo.jpg" alt="">
                        <div class="details">
                            <span>Wakatobie</span>
                            <p>Active Now</p>
                        </div>
                    </div>
                    <a href="#" class="logout">Logout</a>
                </header>
                <div class="search">
                    <span class="text">Select an User to start</span>
                    <input type="text" placeholder="Enter name to search">
                    <button><i class="fa fa-search"></i></button>
                </div>
                <div class="users-list">
                    <a href="">
                        <div class="content">
                            <img src="photo.jpg" alt="">
                            <div class="details">
                                <span>Alice</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fa fa-circle"></i></div>
                    </a>
                    <a href="">
                        <div class="content">
                            <img src="photo.jpg" alt="">
                            <div class="details">
                                <span>Alice</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fa fa-circle"></i></div>
                    </a>
                    <a href="">
                        <div class="content">
                            <img src="photo.jpg" alt="">
                            <div class="details">
                                <span>Alice</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fa fa-circle"></i></div>
                    </a>
                    <a href="">
                        <div class="content">
                            <img src="photo.jpg" alt="">
                            <div class="details">
                                <span>Alice</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fa fa-circle"></i></div>
                    </a>
                    <a href="">
                        <div class="content">
                            <img src="photo.jpg" alt="">
                            <div class="details">
                                <span>Alice</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fa fa-circle"></i></div>
                    </a>
                    <a href="">
                        <div class="content">
                            <img src="photo.jpg" alt="">
                            <div class="details">
                                <span>Alice</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fa fa-circle"></i></div>
                    </a>
                </div>
            </section>
        </div>

        <script src="javascript/user.js"></script>
    </body>
</html>